import Lottie from 'vue-lottie'

export default {
  install: (app) => app.component('Lottie', Lottie),
}
